/*
 * TDQC5
 * Bruce Cain
 *
 * ticker project
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "ticker.h"

int
main(
    int argc,
    char **argv)
{
    FILE           *in_file = NULL;
    company        *stocks = NULL;
    company        *sorted_stocks = NULL;

    if (argc == 2)
    {
        in_file = fopen(argv[1], "r");
    }

    if (in_file != NULL)
    {
        int             ret_value =
            read_input(&stocks, in_file, 1, sort_symbol);
        fclose(in_file);

        if (ret_value)
        {
            fprintf(stderr, "Failed memory allocation in file read.\n");
            exit(-1);
        }
    }

    if (read_input(&stocks, stdin, 0, sort_symbol))
    {
        fprintf(stderr, "Failed memory allocation in stdin read.\n");
        exit(-1);
    }

    tree_sort(&sorted_stocks, stocks, sort_price);
    print_tree(sorted_stocks);
    tree_destroy(sorted_stocks);
    tree_destroy(stocks);
    return 0;
}
