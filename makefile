CC = gcc
CFLAGS = $(CF) -Wall -Wextra -Wpedantic -Wwrite-strings -Wstack-usage=512 -Wfloat-equal -Waggregate-return -Winline
OUT = ticker_driver
DEPS = ticker.h
SRC = ticker.c ticker_driver.c

all: $(DEPS)
	@$(CC) $(CFLAGS) -o $(OUT) $(SRC)

debug: $(DEPS)
	@$(CC) -g $(CFLAGS) -o $(OUT) $(SRC)

profile: $(DEPS)
	@$(CC) -pg $(CFLAGS) -o $(OUT) $(SRC)

clean:
	-@rm -rf *.o
	-@rm -rf gmon.out
	-@rm -rf $(OUT)

