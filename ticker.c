/*
 * TDQC5
 * Bruce Cain
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include "ticker.h"

/**
 * @param keep_running using to control signal interruptions.
 */
static char     keep_running = 1;

/**
 * @func handler
 * @brief determine what happens if signal is caught.
 * @param sig is the signal interrupt code caught.
 */
static void
handler(
    int sig)
{
    fprintf(stderr, "Signal %d caught\n", sig);
    keep_running = 0;
}

/**
 * @func upper
 * @brief Changes a string to upper case.
 * @param string being changed to upper case.
 */
static void
upper(
    char *string)
{
    for (size_t i = 0; i < strlen(string); i++)
    {
        string[i] = toupper(string[i]);
    }
}

int
validate_input(
    char *input,
    char *symbol,
    char *name,
    double *price,
    char is_file)
{
    char            sign[3] = { 0 };
    int             return_value = 1;

    sscanf(input, "%[^#]", input);

    /* Check if parsing file or stdin input */
    if (is_file)
    {
        sscanf(input, "%6s %lf %65[^\n]", symbol, price, name);
    }
    else
    {
        sscanf(input, "%6s %2[^0-9]%lf %65[^\n]", symbol, sign, price, name);

        if (strlen(sign) > 1)
        {
            fprintf(stderr, "Bad sign - %s\nOnly 1 + or -\n", sign);
            return_value = 0;
        }

        if (sign[0] == '-')
        {
            *price *= -1;
        }
    }

    if (symbol[0] == '#')
    {
        return 0;
    }

    if (strlen(symbol) == 6)
    {
        fprintf(stderr, "Bad symbol - %s\nOnly 5 characters or less\n",
                symbol);
        return_value = 0;
    }


    if (strlen(name) == 65)
    {
        fprintf(stderr, "Bad name - %s\nOnly 64 characters or less\n", name);
        return_value = 0;
    }

    if (*price > 100000000 || *price <= -100000000)
    {
        fprintf(stderr, "Bad price - -1,000,000 < %.2f < 1,000,000\n", *price);
        return_value = 0;
    }

    return return_value;
}

void
print_stock(
    company * data)
{
    printf("%s %.2lf %s\n", data->symbol, (double) data->cents / 100,
           data->name);
}

void
print_tree(
    company * root)
{
    if (root == NULL)
    {
        return;
    }

    if (root->left != NULL)
    {
        print_tree(root->left);
    }

    print_stock(root);

    if (root->right != NULL)
    {
        print_tree(root->right);
    }
}

void
tree_insert(
    company ** root,
    company * comp,
    int             (*cmp) (const company * a,
                            const company * b))
{
    if (*root == NULL)
    {
        *root =
            stock_create(comp->symbol, comp->name, (double) comp->cents / 100);
    }
    else if (cmp(comp, *root) == 0)
    {
        if (((*root)->cents / 100) + (comp->cents / 100) < 1000000 &&
            ((*root)->cents) + (comp->cents) > 1)
        {
            (*root)->cents += comp->cents;

            if (strlen(comp->name) != 0)
            {
                strcpy((*root)->name, comp->name);
            }
        }
        else
        {
            fprintf(stderr, "Bad stock value. Range 0.01 to 1000000\n");
        }
    }
    else if (cmp(comp, *root) < 0)
    {
        if ((*root)->left == NULL)
        {
            if (comp->cents < 1)
            {
                fprintf(stderr, "Bad initial stock value. %ld.%ld < 0\n",
                        comp->cents / 100, comp->cents % 100);
                return;
            }

            (*root)->left =
                stock_create(comp->symbol, comp->name,
                             (double) comp->cents / 100);
        }
        else
        {
            tree_insert(&(*root)->left, comp, cmp);
        }
    }
    else
    {
        if ((*root)->right == NULL)
        {
            if (comp->cents < 1)
            {
                fprintf(stderr, "Bad initial stock value. %ld.%ld < 0\n",
                        comp->cents / 100, comp->cents % 100);
                return;
            }

            (*root)->right =
                stock_create(comp->symbol, comp->name,
                             (double) comp->cents / 100);
        }
        else
        {
            tree_insert(&(*root)->right, comp, cmp);
        }
    }
}

company        *
stock_create(
    char *symbol,
    char *name,
    double price)
{
    company        *new_stock = malloc(sizeof(company));

    if (!new_stock)
    {
        return NULL;
    }

    new_stock->left = NULL;
    new_stock->right = NULL;
    new_stock->name = strdup(name);

    if (!new_stock->name)
    {
        free(new_stock);
        return NULL;
    }

    strncpy(new_stock->symbol, symbol, sizeof(new_stock->symbol) - 1);
    new_stock->symbol[sizeof(new_stock->symbol) - 1] = '\0';

    upper(new_stock->symbol);

    new_stock->cents = 100 * price;

    return new_stock;
}

void
tree_destroy(
    company * root)
{
    if (root == NULL)
    {
        return;
    }

    if (root->left != NULL)
    {
        tree_destroy(root->left);
    }

    if (root->right != NULL)
    {
        tree_destroy(root->right);
    }

    free(root->name);
    free(root);
}

int
read_input(
    company ** root,
    FILE * stream,
    char is_file,
    int             (*compare) (const company * a,
                                const company * b))
{
    size_t          size = 0;
    double          price = 0;
    char           *symbol = NULL, *name = NULL, *input = NULL;
    company        *business = NULL;
    struct sigaction *sig_catcher = calloc(1, sizeof(struct sigaction));

    sig_catcher->sa_handler = handler;

    symbol = calloc(7, sizeof(char));

    if (symbol == NULL)
    {
        return -1;
    }

    name = calloc(66, sizeof(char));

    if (name == NULL)
    {
        free(symbol);
        return -1;
    }

    sigaction(SIGINT, sig_catcher, NULL);

    while (getline(&input, &size, stream) != -1 && keep_running == 1)
    {
        if (input == NULL)
        {
            free(symbol);
            free(name);
            return -1;
        }

        if (validate_input(input, symbol, name, &price, is_file))
        {
            business = stock_create(symbol, name, price);

            if (business == NULL)
            {
                fprintf(stderr, "Failed stock allocation.\n");
            }
            else
            {
                tree_insert(root, business, compare);
                free(business->name);
                free(business);
            }
        }

        memset(symbol, 0, 7);
        memset(name, 0, 66);
    }

    free(symbol);
    free(name);
    free(input);
    free(sig_catcher);
    return 0;
}

void
tree_sort(
    company ** new_tree,
    company * unsorted_tree,
    int             (*compare) (const company * a,
                                const company * b))
{
    if (unsorted_tree == NULL)
    {
        return;
    }

    if (unsorted_tree->left != NULL)
    {
        tree_sort(new_tree, unsorted_tree->left, compare);
    }

    tree_insert(new_tree, unsorted_tree, compare);

    if (unsorted_tree->right != NULL)
    {
        tree_sort(new_tree, unsorted_tree->right, compare);
    }
}

int
sort_symbol(
    const company * a,
    const company * b)
{
    return strcmp(a->symbol, b->symbol);
}

int
sort_price(
    const company * a,
    const company * b)
{
    int             return_val = a->cents - b->cents;

    if (return_val == 0)
    {
        return_val = -1;
    }

    return return_val;
}
