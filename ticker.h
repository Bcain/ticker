/*
 * Author: Bruce Cain
 *
 * ticker header file
 */

#ifndef TICKER_H
#define TICKER_H

#include <string.h>

/**
 * A struct to hold each company data.
 */
typedef struct company
{
    /*@{ */
    char            symbol[6];
                            /**< Company symbol */
    ssize_t         cents;  /**< Company price */
    char           *name;   /**< Company name */
    struct company *left;   /**< Left node */
    struct company *right;  /**< Right node */
    /*@} */
} company;

/**
 * @func validate_input
 * @brief Validates file and user input.
 * @param input is the string being parsed.
 * @param symbol is the stocks symbol.
 * @param name is the name of the company (optional).
 * @param price is the price/change of stock.
 */
int             validate_input(
    char *input,
    char *symbol,
    char *name,
    double *price,
    char is_file);

/**
 * @func print_stock
 * @brief Prints a stocks information.
 * @param data A struct that contains a stocks info.
 */
void            print_stock(
    company * data);

/**
 * @func print_tree
 * @brief prints out the nodes of tree.
 * @param root The start of the tree.
 */
void            print_tree(
    company * root);

/**
 * @func tree_insert
 * @brief Inserts node into tree.
 * @param root Start of tree.
 * @param comp node to add into tree.
 * @param cmp function to compare two nodes.
 */
void            tree_insert(
    company ** root,
    company * comp,
    int             (*cmp) (const company * a,
                            const company * b));

/**
 * @func stock_create
 * @brief Creates new company node.
 * @param symbol Company symbol.
 * @param name Company name.
 * @param price Company stock price.
 * @return Returns pointer to company or NULL.
 */
company        *stock_create(
    char *symbol,
    char *name,
    double price);

/**
 * @func tree_destroy
 * @brief Free's memory of the tree.
 * @param Root of tree.
 */
void            tree_destroy(
    company * root);

/**
 * @func read_stdin
 * @brief Read company data from a stdin and build tree.
 * @param root Root of the tree.
 * @param compare Function used to sort tree.
 * @return A 0 for success or -1 for failure.
 */
int             read_input(
    company ** root,
    FILE * stream,
    char is_file,
    int             (*compare) (const company * a,
                                const company * b));

/**
 * @func tree_sort
 * @brief Sorts new tree.
 * @param new_tree Root of new tree
 */
void            tree_sort(
    company ** new_tree,
    company * unsorted_tree,
    int             (*compare) (const company * a,
                                const company * b));

/**
 * @func sort_symbol
 * @brief Sort by company symbol.
 * @param a Left company to compare to b.
 * @param b Right company to compare to a.
 * @return Result of strcmp.
 */
int             sort_symbol(
    const company * a,
    const company * b);

/**
 * @func sort_price
 * @brief Sort by company prices.
 * @param a Left company to compare to b.
 * @param b Right company to compare to a.
 */
int             sort_price(
    const company * a,
    const company * b);
#endif
